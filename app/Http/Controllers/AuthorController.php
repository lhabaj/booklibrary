<?php

namespace App\Http\Controllers;

use App\Models\Author;

class AuthorController extends Controller
{
    public function index() {

        $authors = Author::withCount('books')->orderBy('surname', 'ASC')->paginate(20); // withCount is for count column in author list
        
        return view('author.index', compact('authors'));
    }

    public function create() {

        return view('author.create');
    }

    public function store() {

        if ($this->authorExists()) {
            return back()->withInput()->withErrors(['formError' => ['Author already exists!']]);
        }

        Author::create($this->validatedFormData());

        return redirect('/authors');
    }

    public function show(Author $author) {
        
        return view('author.show', [
            'author' => $author,
            'objectType' => 'author',
            'objectName' => $author->name . ' ' . $author->surname,
        ]);
    }

    public function edit(Author $author) {

        return view('author.edit', compact('author'));
    }

    public function update(Author $author) {

        if (! $this->authorExists()) {
            $author->update($this->validatedFormData());
        }

        return redirect('/authors/' . $author->id);
    }

    public function destroy(Author $author) {

        $author->books()->delete();
        $author->delete();

        return redirect('/authors');
    }

    public function validatedFormData() {

        return request()->validate([
            'name' => 'required|min:2|max:255',
            'surname' => 'required|min:3|max:255',
        ]);
    }

    public function authorExists() {

        return Author::where($this->validatedFormData())->exists();
    }
}
