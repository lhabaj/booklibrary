<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;
use App\Models\Author;

class BookController extends Controller
{
    
    public function index() {

        $books = Book::orderBy('title', 'ASC')->paginate(20);

        return view('book.index', compact('books'));
    }

    public function create() {

        return view('book.create', ['authors' => Author::get()]);
    }

    public function store() {

        $author = Author::where(['id' => $this->validatedFormData()['author_id']])->first();
        $book = new Book($this->validatedFormData());
        $author->books()->save($book);

        return redirect('/books');
    }

    
    public function show(Book $book) {

        return view('book.show', [
            'book' => $book,
            'objectType' => 'book',
            'objectName' => $book->title,
        ]);
    }

    public function edit(Book $book) {

        return view('book.edit', [
            'authors' => Author::get(),
            'book' => $book,
        ]);
    }

    public function update(Book $book) {

        $book->update($this->validatedFormData());

        return redirect('/books/' . $book->id);
    }

    public function destroy(Book $book) {
        
        $book->delete();

        return redirect('/books');
    }

    public function validatedFormData() {

        $data = request()->validate([
            'title' => 'required|min:2|max:255',
            'author_id' => 'required|integer|gt:0',
            'is_borrowed' => 'boolean',
        ]);

        if (! request()->has('is_borrowed')) {
            $data['is_borrowed'] = 0;
        }

        return $data;
    }

    
}
