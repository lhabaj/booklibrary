<?php

namespace App\Http\Controllers;

use App\Models\Book;

class ajaxController extends Controller
{

    public function changeBorrowedStatus() {

        if (request()->ajax()) {
            $data = request()->validate([
                'id' => 'required|integer',
                'is_borrowed' => 'boolean',
            ]);

            $book = Book::where(['id' => $data['id']]);
            $book->update(['is_borrowed' => $data['is_borrowed']]);
        }
    }

}