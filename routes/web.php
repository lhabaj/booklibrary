<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'BookController@index');

Route::get('/books', 'BookController@index');
Route::get('/books/create', 'BookController@create');
Route::post('/books', 'BookController@store');
Route::get('/books/{book}', 'BookController@show');
Route::get('/books/{book}/edit', 'BookController@edit');
Route::put('/books/{book}', 'BookController@update');
Route::delete('/books/{book}', 'BookController@destroy');

Route::get('/authors', 'AuthorController@index');
Route::get('/authors/create', 'AuthorController@create');
Route::post('/authors', 'AuthorController@store');
Route::get('/authors/{author}', 'AuthorController@show');
Route::get('/authors/{author}/edit', 'AuthorController@edit');
Route::put('/authors/{author}', 'AuthorController@update');
Route::delete('/authors/{author}', 'AuthorController@destroy');

// AJAX calls
Route::post('/books/change-borrowed-status', 'AjaxController@changeBorrowedStatus');
