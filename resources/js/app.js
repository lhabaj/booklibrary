require('./bootstrap');


$(function() {

    $('.is_borrowed').on('change', function(){

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            method: "POST",
            url: "/books/change-borrowed-status",
            data: {
                id: $(this).data('book-id'),
                is_borrowed: $(this).prop('checked') ? 1 : 0
            }
        });

    });

});

$(document).on('shown.bs.modal', '#deleteModal', function(){
    $(document).on('click', '#confirmYes', function() {
        $('.delete-form').trigger('submit');
    });
});