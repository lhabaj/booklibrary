@extends('app')

@section('title', 'Edit author')

@section('main')
    <h1>Edit author</h1>

    <form action="/authors/{{ $author->id }}" method="post">
        @csrf
        @method('put')

        <div class="form-group">
            <label for="name">Author's name</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="Enter author's name" value="{{ $author->name }}">
            @error('name')
                <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
        <div class="form-group">
            <label for="surname">Author's surname</label>
            <input type="text" class="form-control" id="surname" name="surname" placeholder="Enter author's surname" value="{{ $author->surname }}">
            @error('surname')
                <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
        
        <p class="text-danger">{{ $errors->first('formError') ?? '' }}</p>

        <button type="submit" class="btn btn-primary">Submit changes</button>
    </form>

@endsection