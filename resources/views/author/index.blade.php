@extends('app')

@section('title', 'All authors')

@section('main')
    
    <h1 class="text-center">Authors</h1>

    <div>
        <a class="btn btn-primary" href="/authors/create">Add new author</a>
    </div>
    <br>
    
    <table class="table">
        <thead>
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Surname</th>
                <th scope="col">Book count</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        @forelse($authors as $author)
            <tr>
                <td>{{ $author->name }}</td> 
                <td>{{ $author->surname }}</td>
                <td>{{ $author->books_count }}</td>
                <td><a href="/authors/{{ $author->id }}">Detail</a></td>
            </tr>
        @empty
            <tr>
                <td colspan=4>No authors available.</td>
            </tr>
        @endforelse
    </table>

    <div class="d-flex justify-content-center">
        {{ $authors->links() }}
    </div>
    
@endsection