@extends('app')

@section('title', 'Author detail')

@section('main')
    
    <h1 class="text-center">Author detail</h1>

    <table class="table">
        <thead>
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Surname</th>
                <th scope="col">Book count</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{ $author->name }}</td> 
                <td>{{ $author->surname }}</td>
                <td>{{ $author->books_count ?? 0 }}</td>
                <td><a href="/authors/{{ $author->id }}/edit">Edit</a></td>
                <td>
                    <form class="delete-form" action="/authors/{{ $author->id }}" method="post">
                        @csrf
                        @method('delete')
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#deleteModal">Delete</button>
                    </form>
                </td>
            </tr>
        </tbody>
    </table>

    @include('partials.modal')
    
@endsection