@extends('app')

@section('title', 'Add new author')

@section('main')

    <h1>Add new author</h1>

    <form action="/authors" method="post">
        @csrf

        <div class="form-group">
            <label for="name">Author's name</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="Enter author's name" value="{{ old('name') }}">
            @error('name')
                <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
        <div class="form-group">
            <label for="surname">Author's surname</label>
            <input type="text" class="form-control" id="surname" name="surname" placeholder="Enter author's surname" value="{{ old('surname') }}">
            @error('surname')
                <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
        
        <p class="text-danger">{{ $errors->first('formError') ?? '' }}</p>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

@endsection