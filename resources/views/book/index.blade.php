@extends('app')

@section('title', 'All books')

@section('main')
    
    <h1 class="text-center">Books</h1>

    <div>
        <a class="btn btn-primary" href="/books/create">Add new book</a>
    </div>
    <br>

    <table class="table">
        <thead>
            <tr>
                <th scope="col">Title</th>
                <th scope="col">Author</th>
                <th scope="col">Borrowed</th>
                <th scope="col"></th>
            </tr>
        </thead>
        <tbody>
            @forelse($books as $book)
                <tr>
                    <td>{{ $book->title }}</td> 
                    <td>{{ $book->author->name }} {{ $book->author->surname }}</td>
                    <td>
                        <input type="checkbox" name="is_borrowed" class="is_borrowed" value="1" data-book-id="{{ $book->id }}" @if($book->is_borrowed)checked="checked"@endif>
                    </td>
                    <td><a href="/books/{{ $book->id }}">Detail</a></td>
                </tr>
            @empty
                <tr>
                    <td colspan=4>No books available.</td>
                </tr>
            @endforelse
        <tbody>
    </table>

    <div class="d-flex justify-content-center">
        {{ $books->links() }}
    </div>
    
@endsection

