@extends('app')

@section('title', 'Book detail')

@section('main')
    
    <h1 class="text-center">Book detail</h1>

    <table class="table">
        <thead>
            <tr>
                <th scope="col">Title</th>
                <th scope="col">Author</th>
                <th scope="col">Borrowed</th>
                <th scope="col"></th>
                <th scope="col"></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{ $book->title }}</td> 
                <td>{{ $book->author->name }} {{ $book->author->surname }}</td>
                <td>
                    <input type="checkbox" name="is_borrowed" class="is_borrowed" value="1" data-book-id="{{ $book->id }}" @if($book->is_borrowed)checked="checked"@endif>
                </td>
                <td><a href="/books/{{ $book->id }}/edit">Edit</a></td>
                <td>
                    <form class="delete-form" action="/books/{{ $book->id }}" method="post">
                        @csrf
                        @method('delete')
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#deleteModal">Delete</button>
                    </form>
                </td>
            </tr>
        </tbody>
    </table>

    @include('partials.modal')

@endsection