@extends('app')

@section('title', 'Add new book')

@section('main')
    <h1>Add new book</h1>

    <form action="/books" method="post">
        @csrf

        <div class="form-group">
            <label for="author_id">Book author</label>
            <select class="form-control" id="author_id" name="author_id">
                <option>Choose</option>
                @forelse($authors as $author)
                    <option value="{{ $author->id }}">{{ $author->name }} {{ $author->surname }}</option>
                @empty
                    <option>No authors available</option>
                @endforelse
            </select>
            @error('author_id')
                <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>

        <div class="form-group">
            <label for="title">Book title</label>
            <input type="text" class="form-control" id="title" name="title" placeholder="Enter book title">
            @error('title')
                <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
        
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

@endsection