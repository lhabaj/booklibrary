@extends('app')

@section('title', 'Edit book')

@section('main')
    <h1>Edit book</h1>

    <form action="/books/{{ $book->id }}" method="post">
        @csrf
        @method('put')

        <div class="form-group">
            <label for="author_id">Book author</label>
            <select class="form-control" id="author_id" name="author_id">
                <option>Choose</option>
                @forelse($authors as $author)
                    <option value="{{ $author->id }}" @if($book->author_id === $author->id)selected="selected"@endif>{{ $author->name }} {{ $author->surname }}</option>
                @empty
                    <option>No authors available</option>
                @endforelse
            </select>
            @error('author_id')
                <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>

        <div class="form-group">
            <label for="title">Book title</label>
            <input type="text" class="form-control" id="title" name="title" placeholder="Enter book title" value="{{ $book->title }}">
            @error('title')
                <span class="text-danger">{{ $message }}</span>
            @enderror
        </div>
        
        <div class="form-group">
            <div class="form-check">
                <input type="checkbox" class="form-check-input" id="is_borrowed" name="is_borrowed" value="1" @if($book->is_borrowed)checked="checked"@endif>
                <label class="form-check-label" for="is_borrowed">Borrowed</label>
            </div>
        </div>

        <button type="submit" class="btn btn-primary">Submit changes</button>
    </form>

@endsection